import subprocess, os

class dockerEngine:
    dockers = {}

    def __init__(self, configFile):
        with open(configFile, "r") as f:
            self.dockers = eval(f.read().strip())

    def getSanitizedName(self, name):
        for category in self.dockers:
            for chall in self.dockers[category]:
                if (chall.lower().startswith(name.lower())):
                    return chall.lower()

        return ""

    def getChallengeByName(self, name):
        for category in self.dockers:
            for chall in self.dockers[category]:
                if (chall.lower().startswith(name.lower())):
                    return self.dockers[category][chall]

        return []

    def startDocker(self, name):
        chall = self.getChallengeByName(name)
        if (chall == []): return -1

        ports = " ".join(['-p {}:{}'.format(p, chall[0][p]) for p in chall[0]])

        if ("mem" in chall[1]): mem = int(chall[1]["mem"])
        else: mem = 100

        if ("cpu" in chall[1]): cpu = float(chall[1]["cpu"])
        else: cpu = 0.1

        extra_args = ""
        if len(chall) > 2:
            extra_args = chall[2]
            out_ports = [p for p in chall[0]]

            extra_args = extra_args.replace("$PORT", out_ports[0])
            extra_args = extra_args.replace("$HTTP_PORT", out_ports[1])

        print(extra_args)
        cmd = 'docker run -d --cpus="{1}" --memory="{2}m" --name {0} {4} {3} --restart always {0}'.format(name, cpu, mem, ports, extra_args)
        print("running" + cmd)
        return os.system(cmd)

    def stopDocker(self, name):
        chall = self.getChallengeByName(name)
        if (chall == []): return

        cmd = 'docker rm -f {}'.format(name)
        return os.system (cmd)

    def getConnectionsForDocker(self, name):
        chall = self.getChallengeByName(name)
        if (chall == []): return -1

        pid = subprocess.check_output(["docker inspect -f '{{.State.Pid}}' " + name], shell = True).decode("ASCII").strip()
        numConnections = subprocess.check_output(["nsenter -t {} -n netstat -anp | grep ESTABLISHED | wc -l".format(pid)], shell = True).decode ("ASCII").strip()
        return int(numConnections)

    def getAllDockers(self):
        return self.dockers

    def getRunningDockers(self):
        f = subprocess.check_output(["docker", "container", "ls"]).decode("UTF-8").strip().split("\n")[1:]

        runningChallenges = []
        runningDict = {}

        for line in f:
            line = line.split()[-1]
            runningChallenges.append (line)

        for category in self.dockers:
            for chall in self.dockers[category]:
                if (chall in runningChallenges):
                    if (category not in runningDict):
                        runningDict[category] = []

                    runningDict[category].append(chall)

        return runningDict

    def isChallengeRunning(self, name):
        f = subprocess.check_output(["docker", "container", "ls"]).decode("UTF-8").strip().split("\n")[1:]

        for line in f:
            line = line.split()[-1]
            if (line == name):
                return True

        return False