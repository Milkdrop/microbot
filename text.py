cmds = {
	"help": "Show this message",
	"challs": "See available challenges",
	"restart chall": "Vote to restart a challenge"
}

cmdsAdmin = {
	"help": "Show this message",
	"refresh": "Refresh challenge database",
	"challenge": "Display challenge server status",
	"platform": "Display platform server status"
}

cmdsDocker = {
	"challs": "Display all available dockers",
	"status [cat1 cat2 ...]": "Display running dockers (filterable with cat1, cat2, ...)",
	"start chall1 chall2 ...": "Start dockers for challenges (chall1, chall2, ...)",
	"stop chall1 ...": "Stop dockers for challenges",
	"restart chall1 ...": "Restart dockers for challenges",
	"build/rebuild chall1 chall2 ...": "Rebuild dockers for challenges",
	"rebuildall": "Rebuild all dockers"
}

success = ":white_check_mark:"
fail = ":x: Error."