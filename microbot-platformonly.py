import discord, sys, requests, asyncio
import utils

prefix = "!"
adminChannels = [523194184140849158]
loggingChannel = 784159313798889473
lastScoreboardTeamCount = 0

client = discord.Client()

@client.event
async def on_ready():
	print('All good! Name: ' + client.user.name)
	await client.change_presence(activity = discord.Game(name = 'I oversee dockers. !help'))

@client.event
async def on_message(message):
	global engine
	if (message.author == client.user): return
	
	inp = message.content.lower().strip()

	if (message.channel.id in adminChannels and inp == prefix + "platform"):
		output = ":gear: Platform Server Status:\n"
		output += utils.getSysStatus()
		await message.channel.send(output)

async def monitor_scoreboard():
	global lastScoreboardTeamCount
	
	while True:
		scoreboard = requests.get("https://xmas.htsp.ro/scores").text
		ind = scoreboard.find("href=\"user?id=")
		usercnt = 0

		while (ind != -1):
			usercnt += 1
			scoreboard = scoreboard[ind + 1:]
			ind = scoreboard.find("href=\"user?id=")

		if (usercnt != lastScoreboardTeamCount):
			lastScoreboardTeamCount = usercnt

			# Wait for client to be available
			while (not client.is_ready()):
				await asyncio.sleep(1)

			await client.get_channel(loggingChannel).send("**" + str(usercnt) + "** registered users on platform.")
		
		await asyncio.sleep(60 * 60)

if ("--score-monitor" in sys.argv[1:]):
	client.loop.create_task(monitor_scoreboard())

token = open(utils.getpwd() + "/token", "r").read().strip()
client.run (token)