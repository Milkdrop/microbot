import os, psutil, subprocess, string

def normalizeString(s):
	return ''.join([x for x in s.lower() if x in string.ascii_lowercase])
	
def getpwd():
	return os.path.dirname(os.path.realpath(__file__))

def getSysStatus ():
	mem = psutil.virtual_memory ()
	memUsed = round (mem.used / (1024 ** 2))
	memTotal = round (mem.total / (1024 ** 2))

	numConnections = subprocess.check_output (["netstat -anp | grep ESTABLISHED | wc -l"], shell = True).decode ("ASCII").strip ()

	sysStatus = {"cpu": round (psutil.cpu_percent (), 2), "memUsed": memUsed, "memTotal": memTotal, "numConnections": numConnections}

	output = "**{}%** CPU Usage\n".format (sysStatus["cpu"])
	output += "**{}/{}** MB RAM Used\n".format (sysStatus["memUsed"], sysStatus["memTotal"])
	output += "**{}** Connections\n".format (sysStatus["numConnections"])

	return output