import os, sys, subprocess, logging, time
import disnake, platform, asyncio, requests
import utils, text
from dockerEngine import dockerEngine

prefix = "!"
version = "1.0 Beta"
dockersFile = "dockers.txt"
pathToDockerfiles = "/root/xmas-2021"
maxvotes = 5
adminChannels = [523194184140849158]
publicChannels = [655038385513300018]
forbidden_challenge_restarts = ["paint"]
restartreq = {}

if (os.getuid() != 0):
    print("I need root.")
    exit(-1)

client = disnake.Client()

@client.event
async def on_ready():
    print('All good! Name: ' + client.user.name)
    await client.change_presence(activity = disnake.Game(name = 'I oversee dockers! Use !help'))

@client.event
async def on_message (message):
    global engine
    if (message.author == client.user): return

    authorID = message.author.id
    inp = message.content.lower().split ()
    if (inp[0][:len(prefix)] != prefix): return

    inp[0] = inp[0][len(prefix):]
    output = ""

    if (message.channel.id in publicChannels):
        if (inp[0] == "help"):
            output = ":gear: Microbot **v{}**\nHere are the commands you can run:\n".format(version)

            for cmd in text.cmds:
                output += "\t**- {}:** `{}`\n".format (cmd, text.cmds[cmd])

        elif (inp[0] == "challs"):
            running = engine.getRunningDockers()
            
            if (len(running) != 0):
                output = ":gear: Available challenges:\n"
                for category in running:
                    output += "\t**- {} ({}):** {}\n".format(category, len(running[category]),
                        ' '.join(['`{}`'.format(c) for c in running[category]]))
            
            else:
                output = ":gear: No available challenges."

        elif (inp[0] == "restart"):
            challName = engine.getSanitizedName(''.join(inp[1:]))

            if (challName not in forbidden_challenge_restarts):
                chall = engine.getChallengeByName(challName)
                dockers = engine.getRunningDockers()

                challs = []
                for c in dockers:
                    for s in dockers[c]:
                        challs.append(s)

                if (chall == [] or challName not in challs):
                    output = "No such challenge: `" + utils.normalizeString(''.join(inp[1:])) + "`"
                else:
                    if (challName not in restartreq):
                        restartreq[challName] = []

                    if (authorID not in restartreq[challName]):
                        restartreq[challName].append (authorID)

                    votes = len(restartreq[challName])

                    if (votes < maxvotes):
                        output = "**{}/{}** votes needed to restart `{}`".format (votes, maxvotes, challName)
                    else:
                        output = "Restarting {}: ".format(challName)
                        msg = await sendMsg(message.channel, output + "...")
                        code = engine.stopDocker(challName)
                        code = engine.startDocker(challName)

                        if (code == 0):
                            await msg.edit(content = output + " " + text.success)
                        else:
                            await msg.edit(content = output + " " + text.fail)

                        output = ""
                        restartreq[challName] = []
            
            else:
                output = "That challenge cannot be restarted automatically!"

    elif (message.channel.id in adminChannels):
        if (inp[0] == "help"):
            output = ":gear: Microbot **v{}**\nHere are the commands you can run:\n".format(version)

            for cmd in text.cmdsAdmin:
                output += "\t**- {}:** `{}`\n".format (cmd, text.cmdsAdmin[cmd])

            output += "\n:gear: Docker Commands:\n"
            for cmd in text.cmdsDocker:
                output += "\t**- {}:** `{}`\n".format (cmd, text.cmdsDocker[cmd])

        elif (inp[0] == "refresh"):
            if (len(message.attachments) > 0):
                r = requests.get(message.attachments[0].url, allow_redirects=True)
                with open(utils.getpwd() + "/" + dockersFile, "w") as f:
                    f.write(r.text)

            engine = dockerEngine(utils.getpwd() + "/" + dockersFile)
            output = "Challenge database has been refreshed"

        # This could be moved to the docker engine...
        elif (inp[0] == "build" or inp[0] == "rebuild" or inp[0] == "buildall" or inp[0] == "rebuildall"):
            output = "Pulling from git: "
            msg = await sendMsg(message.channel, output + "...")
            code = os.system("cd {} && git reset --hard && git pull".format(pathToDockerfiles))

            if (code == 0):
                output += ":white_check_mark:\n"
            else:
                output += ":x: Error.\n"

            await(msg.edit(content = output))

            output += "Rebuilding dockers:\n"
            dockers = engine.getAllDockers()

            if (inp[0] == "rebuildall"):
                challs = []
                
                for c in dockers:
                    for s in dockers[c]:
                        challs.append(s)
            else:
                challs = inp[1:]

            for chall in challs:
                challName = engine.getSanitizedName(chall)
                if (len(challName) == 0):
                    output += "\t**- (invalid challenge name):** :x: Error.\n"
                    await(msg.edit(content = output))
                    continue

                output += "\t**- {}:** ".format(challName)
                await(msg.edit(content = output + "..."))
                code = 0
                folder = ""

                for category in dockers:
                    if (challName in dockers[category]):
                        folder = pathToDockerfiles + "/" + category + "/" + challName
                        break

                if (folder == "" or not os.path.exists(folder)):
                    code = -1
                else:
                    if (os.path.exists(folder + "/Dockerfile")):
                        code = os.system("cd {} && docker build -t {} .".format(folder, challName))
                    elif (os.path.exists(folder + "/build_docker.sh")):
                        code = os.system("cd {} && ./build_docker.sh".format(folder))
                    else:
                        code = -2

                if (code == 0):
                    output += text.success

                    if (engine.isChallengeRunning(challName)):
                        output += " Auto-restarting chall: "
                        await(msg.edit(content = output + "..."))

                        code = engine.stopDocker(challName)
                        code = engine.startDocker(challName)

                        if (code == 0):
                            output += text.success

                            chall = engine.getChallengeByName(challName)
                            ports = [int(p) for p in chall[0]]

                            if (len(ports) == 1):
                                output += " Port: " + str(ports[0])
                            else:
                                output += " Ports: " + str(ports)
                                
                        else:
                            output += text.fail

                    output += "\n"
                elif (code == -1):
                    output += ":x: Could not find challenge folder.\n"
                elif (code == -2):
                    output += ":x: Could not find Dockerfile or build_docker.sh\n"
                else:
                    output += text.fail + "\n"

                await(msg.edit(content = output))

            output = ""

        elif (inp[0] == "challenge"):
            output = ":gear: Challenge Server Status:\n"
            output += utils.getSysStatus()
        
        # let the other instance respond
        elif (inp[0] == "platform"):
            return

        elif (inp[0] == "start" or inp[0] == "stop" or inp[0] == "restart"):
            if (inp[0] == "start"): output = "Starting challenge(s):\n"
            if (inp[0] == "stop"): output = "Stopping challenge(s):\n"
            if (inp[0] == "restart"): output = "Restarting challenge(s):\n"
            msg = await sendMsg(message.channel, output)

            for challName in inp[1:]:
                challName = engine.getSanitizedName(challName)
                if (len(challName) == 0):
                    output += "\t**- (invalid challenge name):** :x: Error.\n"
                    await(msg.edit(content = output))
                    continue

                output += "\t**- {}:**".format(challName)
                await msg.edit(content = output + " ...")

                if (inp[0] == "start"):
                    if (engine.isChallengeRunning(challName)):
                        code = 0
                    else:
                        code = engine.startDocker(challName)

                if (inp[0] == "stop"): 
                    if (not engine.isChallengeRunning(challName)):
                        code = 0
                    else:
                        code = engine.stopDocker(challName)

                if (inp[0] == "restart"):
                    code = engine.stopDocker(challName)
                    code = engine.startDocker(challName)

                if (code == 0):
                    output += " " + text.success

                    if (inp[0] == "start" or inp[0] == "restart"):
                        chall = engine.getChallengeByName(challName)
                        ports = [int(p) for p in chall[0]]

                        if (len(ports) == 1):
                            output += " Port: " + str(ports[0])
                        else:
                            output += " Ports: " + str(ports)

                    output += "\n"

                else:
                    output += " " + text.fail + "\n"

                await msg.edit(content = output)

            output = ""

        elif (inp[0] == "challs"):
            dockers = engine.getAllDockers()
            if (len(dockers) != 0):
                output = "Available challenges:\n"

                for category in dockers:
                    output += "\t**- {} ({}):** {}\n".format(category, len(dockers[category]),
                        ' '.join(['`{}`'.format(c) for c in dockers[category]]))
            
            else:
                output = "No available challenges."

        elif (inp[0] == "status"):
            running = engine.getRunningDockers()

            filteredCategories = []
            for filteredCategory in inp[1:]:
                filteredCategory = filteredCategory.lower()
                for category in running:
                    if (category.startswith(filteredCategory)):
                        filteredCategories.append(category)

            if (len(running) != 0):
                output = ":gear: Running challenges:\n"
                totalConnections = 0

                for category in running:
                    if (len(filteredCategories) > 0 and category not in filteredCategories):
                        continue

                    output += "\t**- {} ({}):**\n".format(category, len(running[category]))

                    for challenge in running[category]:
                        connections = engine.getConnectionsForDocker(challenge)

                        output += "\t\t- `{}`: {} players | ports: {}\n".format(
                            challenge, connections, [int(p) for p in engine.getChallengeByName(challenge)[0]]
                        )

                        totalConnections += connections

                output += "\nTotal Connections: **{}**".format(totalConnections)
                
            else:
                output = "No running challenges."
                    
        else:
            output = "Unknown command: `{}`".format(inp[0])

    if (len(output) != 0):
        await sendMsg(message.channel, output)

async def sendMsg(channel, msg):
    return await channel.send(msg)

token = open(utils.getpwd() + "/token", "r").read().strip()
engine = dockerEngine(utils.getpwd () + "/" + dockersFile)
client.run (token)